---
source: sections/_api_builder_forms_button/buttonType.md
title: buttonType
bookmark: button-type
description: Retrieves or specifies the type.
endpoint: property
signature: true
---

#### Signature
##### **buttonType**: *"normal" | "accept" | "warning" | "cancel"*
{: .signature }
