---
source: sections/_api_builder_forms_button/on.md
title: "on"
bookmark: "on"
description: Specifies the tap function.
endpoint: method
signature: true
---

#### Signature
##### **on**(tap: *function*): *this*
{: .signature }

`tap` function
: The tap function that is invoked when the button is tapped. The `Button` instance is supplied as argument to the function.

#### Returns
Returns a reference to the control to allow chaining.
