---
source: sections/_api_builder_templates/required.md
title: required
bookmark: required
description: Adds the data required feature.
endpoint: method
signature: true
---

#### Signature
##### **required**(slot: *Slot | undefined*, controller: *BuilderController*, label: *string*): *Feature | undefined*
{: .signature }

`slot` Slot | undefined
: Reference to a slot.

`controller` BuilderController
: Reference to the controller.

`label` string
: Specifies the label for the feature.

#### Returns
Returns a reference to the feature.
