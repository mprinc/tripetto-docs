---
source: sections/_api_module_slots_numeric/minimum.md
title: minimum
bookmark: minimum
description: Contains the minimal value.
endpoint: property
signature: true
---

#### Signature
##### **minimum**?: *number*
