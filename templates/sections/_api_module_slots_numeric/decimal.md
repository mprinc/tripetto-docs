---
source: sections/_api_module_slots_numeric/decimal.md
title: decimal
bookmark: decimal
description: Contains the decimal sign.
endpoint: property
signature: true
---

#### Signature
##### **decimal**?: *string*
