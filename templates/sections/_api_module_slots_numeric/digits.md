---
source: sections/_api_module_slots_numeric/digits.md
title: digits
bookmark: digits
description: Contains the number of digits.
endpoint: property
signature: true
---

#### Signature
##### **digits**?: *number*
