---
source: sections/_api_builder_forms_upload/onInit.md
title: onInit
bookmark: on-init
description: Initializes the upload control.
endpoint: method
signature: true
---

#### Signature
##### **onInit**(): *boolean*
{: .signature }

#### Returns
Returns the intialization state.
