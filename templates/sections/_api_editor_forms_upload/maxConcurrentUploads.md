---
source: sections/_api_builder_forms_upload/maxConcurrentUploads.md
title: maxConcurrentUploads
bookmark: max-concurrent-uploads
description: Specifies the maximum number of files that can be uploaded (default is `3`).
endpoint: method
signature: true
---

#### Signature
##### **maxConcurrentUploads**(uploads: *number | "unlimited"*): *this*
{: .signature }

`uploads` number | "unlimited"
: Specifies the maximum number of concurrent uploads.

#### Returns
Returns a reference to the control to allow chaining.
