---
source: sections/_api_builder_forms_upload/files.md
title: files
bookmark: files
description: Retrieves the array with uploaded files.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **files**: *IUploadFile[]*
{: .signature }
