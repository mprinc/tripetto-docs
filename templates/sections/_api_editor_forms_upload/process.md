---
source: sections/_api_builder_forms_upload/process.md
title: process
bookmark: process
description: Triggers the upload process.
endpoint: method
signature: true
---

#### Signature
##### **process**(delay?: *boolean*): *void*
{: .signature }

`delay` Optional boolean
: Specifies if the upload should start with a delay (defaults to `false` if omitted).

#### Returns
Nothing
