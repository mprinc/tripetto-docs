---
source: sections/_api_builder_forms_email/constructor.md
title: constructor
bookmark: constructor
description: Creates a new email instance.
endpoint: constructor
signature: true
---

#### Signature
##### new Forms.**Email**(value?: *TBinding\<string\>*, style?: *ITextStyle*): *Email*
{: .signature }

`value` Optional TBinding\<string\>
: Specifies the initial value (defaults to `""` if omitted).

`style` Optional ITextStyle
: Specifies the optional style.

#### Returns
`Email`
