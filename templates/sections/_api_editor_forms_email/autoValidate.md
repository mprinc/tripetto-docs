---
source: sections/_api_builder_forms_email/autoValidate.md
title: autoValidate
bookmark: auto-validate
description: Enables automatic control validation.
endpoint: method
signature: true
---

#### Signature
##### **autoValidate**(validation?: *TEmailValidation*): *this*
{: .signature }

`validation` Optional TEmailValidation
: Specifies the validator.

#### Returns
Returns a reference to the control to allow chaining.
