---
source: sections/_api_module_slots_slot/constructor.md
title: constructor
bookmark: constructor
description: Creates a new slot instance.
endpoint: constructor
signature: true
---

#### Signature
##### new **Slot**(type: *string*, kind: *Kinds*, reference: *string*, sequence: *number*): *Slot*
{: .signature }

`type` string
: Specifies the type.

`kind` Kinds
: Specifies the kind.

`reference` string
: Specifies the reference for the slot.

`sequence` number
: Specifies the sequence number.

#### Returns
`Slot`
