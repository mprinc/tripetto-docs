---
source: sections/_api_module_slots_slot/alias.md
title: alias
bookmark: alias
description: Contains an alias for the slot.
endpoint: property
signature: true
---

#### Signature
##### **alias**?: *string*
{: .signature }
