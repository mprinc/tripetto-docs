---
source: sections/_api_module_slots_slot/serialize.md
title: serialize
bookmark: serialize
description: Serializes a slot.
endpoint: method
signature: true
---

#### Signature
##### **serialize**(): *ISlot\<T\>*
{: .signature }

#### Returns
Returns a reference to the serialized slot.
