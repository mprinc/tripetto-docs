---
source: sections/_api_module_slots_slot/label.md
title: label
bookmark: label
description: Contains the label of the slot.
endpoint: property
signature: true
---

#### Signature
##### **label**?: *string*
{: .signature }
