---
source: sections/_api_module_slots_slot/default.md
title: default
bookmark: default
description: Contains the default slot data value.
endpoint: property
signature: true
---

#### Signature
##### **default**?: *T*
