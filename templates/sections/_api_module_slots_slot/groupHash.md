---
source: sections/_api_module_slots_slot/groupHash.md
title: groupHash
bookmark: group-hash
description: Retrieves the group hash if the slot is in a group.
endpoint: method
signature: true
---

#### Signature
##### **groupHash**(parent: *string*): *string*
{: .signature }

`parent` string
: Specifies the parent identifier or hash.

#### Returns
Returns the group hash or an empty string if the slot is not in a group.
