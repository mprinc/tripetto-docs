---
source: sections/_api_module_slots_slot/kind.md
title: kind
bookmark: kind
description: Retrieves the slot kind.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **kind**(): *Kinds*
{: .signature }
