---
source: sections/_api_module_slots_slot/exportable.md
title: exportable
bookmark: exportable
description: Contains if this slot is exportable.
endpoint: property
signature: true
---

#### Signature
##### **exportable**?: *boolean*
{: .signature }
