---
source: sections/_api_module_slots_date/maximum.md
title: maximum
bookmark: maximum
description: Contains the maximum date.
endpoint: property
signature: true
---

#### Signature
##### **maximum**?: *Date | number*
{: .signature }
