---
source: sections/_api_module_slots_boolean/toString.md
title: toString
bookmark: to-string
description: Converts the supplied data to a valid string.
endpoint: method
signature: true
---

#### Signature
##### **toString**(data: *TSerializeTypes | Date*): *string*
{: .signature }

`data` TSerializeTypes | Date
: Specifies the data.

#### Returns
Returns the data as a string.
