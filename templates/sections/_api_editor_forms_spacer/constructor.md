---
source: sections/_api_builder_forms_spacer/constructor.md
title: constructor
bookmark: constructor
description: Creates a new spacer instance.
endpoint: constructor
signature: true
---

#### Signature
##### new Forms.**Spacer**(type: *"large" | "small"*, style?: *ISpacerStyle*): *Spacer*
{: .signature }

`type` "large" | "small"
: Specifies the spacer type (defaults to `large` if omitted).

`style` Optional ISpacerStyle
: Specifies the optional style.

#### Returns
`Spacer`
