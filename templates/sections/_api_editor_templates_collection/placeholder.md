---
source: sections/_api_builder_templates_collection/placeholder.md
title: placeholder
bookmark: placeholder
description: Retrieves the item placeholder.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **placeholder**: *string*
{: .signature }
