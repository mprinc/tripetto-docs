---
source: sections/_api_builder_templates_collection/measure.md
title: measure
bookmark: measure
description: Measures the size of the card.
endpoint: method
signature: true
---

#### Signature
##### **measure**(): *boolean*
{: .signature }

#### Returns
Returns `true` if the size is changed.
