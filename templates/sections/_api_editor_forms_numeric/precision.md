---
source: sections/_api_builder_forms_numeric/precision.md
title: precision
bookmark: precision
description: Specifies the precision.
endpoint: method
signature: true
---

#### Signature
##### **precision**(precision: *number*): *this*
{: .signature }

`precision` number
: Specifies the number of precision digits.

#### Returns
Returns a reference to the control to allow chaining.
