---
source: sections/_api_builder_forms_numeric/suffix.md
title: suffix
bookmark: suffix
description: Specifies the suffix.
endpoint: method
signature: true
---

#### Signature
##### **suffix**(suffix: *string*): *this*
{: .signature }

`suffix` string
: Specifies the suffix string.

#### Returns
Returns a reference to the control to allow chaining.
