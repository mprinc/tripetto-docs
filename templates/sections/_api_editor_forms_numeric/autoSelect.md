---
source: sections/_api_builder_forms_numeric/autoSelect.md
title: autoSelect
bookmark: auto-select
description: Specifies if the number needs to be selected automatically.
endpoint: method
signature: true
---

#### Signature
##### **autoSelect**(select?: *"no" | "focus" | "auto-focus"*): *this*
{: .signature }

`select` "no" | "focus" | "auto-focus"
: Specifies if the number needs to be selected (defaults to `focus` if omitted).

#### Returns
Returns a reference to the control to allow chaining.
