---
source: sections/_api_builder_forms_numeric/alignLeftOnFocus.md
title: alignLeftOnFocus
bookmark: align-left-on-focus
description: Specifies if the alignment should be set to the left on focus.
endpoint: method
signature: true
---

#### Signature
##### **alignLeftOnFocus**(alignLeftOnFocus: *boolean*): *this*
{: .signature }

`alignLeftOnFocus` boolean
: Specifies if the alignment should be set to left on focus.

#### Returns
Returns a reference to the control to allow chaining.
