---
source: sections/_api_module_num/range.md
title: range
bookmark: range
description: Adjusts a number so it is in range between the specifies minimum and maximum.
endpoint: function
signature: true
code: |
  ``` typescript
  range(20, 5, 10); // Returns `10`
  range(0, 5, 10); // Returns `5`
  ```
---

#### Signature
##### Num.**range**(value: *number*, min: *number*, max?: *number*): *number*
{: .signature }

`value` number
: Input value as a number or string.

`min` number
: Specifies the minimum value.

`max` Optional number
: Specifies the maximum value.

#### Returns
Returns the ranged number.
