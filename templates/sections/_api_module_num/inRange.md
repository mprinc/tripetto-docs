---
source: sections/_api_module_num/inRange.md
title: inRange
bookmark: in-range
description: Checks if the given value is within the specified range.
endpoint: function
signature: true
code: |
  ``` typescript
  inRange(5, 5, 10); // Returns `true`
  inRange(0, 5, 10); // Returns `false`
  ```
---

#### Signature
##### Num.**inRange**(value: *number*, min: *number*, max: *number*, edgeMin?: *boolean*, edgeMax?: *boolean*): *boolean*
{: .signature }

`value` number
: Input number.

`min` number
: Specifies the minimum value.

`max` number
: Specifies the maximum value.

`edgeMin` Optional string
: Specifies if the edge of the min value is allowed (defaults to `true` if omitted).

`edgeMax` Optional string
: Specifies if the edge of the max value is allowed (defaults to `false` if omitted).

#### Returns
Returns `true` if the value is in range.
