---
source: sections/_api_module_num/min.md
title: min
bookmark: min
description: Compares two numbers and returns the lowest value.
endpoint: function
signature: true
code: |
  ``` typescript
  min(1, 2); // Returns `1`
  ```
---

#### Signature
##### Num.**min**(a: *number*, b: *number*): *number*
{: .signature }

`a` number
: Input number A.

`b` number
: Input number B.

#### Returns
Returns the number with the lowest value.
