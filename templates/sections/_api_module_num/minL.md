---
source: sections/_api_module_num/minL.md
title: minL
bookmark: min-l
description: Compares the supplied arguments returns the lowest value.
endpoint: function
signature: true
code: |
  ``` typescript
  minL(1, 2, 5, 3); // Returns `1`
  ```
---

#### Signature
##### Num.**minL**(...pArguments: *number[]*): *number*
{: .signature }

`...pArguments` number[]
: Arguments to compare.

#### Returns
Returns the number with the lowest value.
