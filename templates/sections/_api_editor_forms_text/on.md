---
source: sections/_api_builder_forms_text/on.md
title: "on"
bookmark: "on"
description: Specifies the function which is invoked when the input value changes.
endpoint: method
signature: true
---

#### Signature
##### **on**\<T\>(change: *function*): *this*
{: .signature }

`change` function
: Specifies the change function. The `T` instance is supplied as argument to the function.

#### Returns
Returns a reference to the control to allow chaining.
