---
source: sections/_api_builder_forms_radiobutton/optionVisible.md
title: optionVisible
bookmark: option-visible
description: Sets or retrieves the visibility of the specified button option (or the current option if no option value is specified).
endpoint: method
signature: true
---

#### Signature
##### **optionVisible**(value?: *T*, visible?: *boolean*): *boolean*
{: .signature }

`value` Optional T
: Specifies the value of the option to get or set.

`visible` Optional boolean
: Specifies the visibility.

#### Returns
Returns the visibility.
