---
source: sections/_api_builder_forms_radiobutton/on.md
title: "on"
bookmark: "on"
description: Specifies the function which is invoked when the radiobutton selection is changed.
endpoint: method
signature: true
---

#### Signature
##### **on**(change: *function*): *this*
{: .signature }

`change` function
: Specifies the change function. The `Radiobutton<T>` instance is supplied as argument to the function.

#### Returns
Returns a reference to the control to allow chaining.
