---
source: sections/_api_builder_forms_radiobutton/constructor.md
title: constructor
bookmark: constructor
description: Creates a new radio button instance.
endpoint: constructor
signature: true
---

#### Signature
##### new Forms.**Radiobutton**(buttons: *IButton\<T\>[]*, value: *TBinding\<T\>*, style?: *IRadiobuttonStyle*): *Radiobutton*
{: .signature }

`buttons` IButton\<T\>[]
: Specifies the option buttons.

`value` TBinding\<T\>
: Specifies the initially selected value.

`style` Optional IRadiobuttonStyle
: Specifies the optional style.

#### Returns
`Radiobutton`
