---
source: sections/_api_builder_forms_notification/constructor.md
title: constructor
bookmark: constructor
description: Creates a new notification instance.
endpoint: constructor
signature: true
---

#### Signature
##### new Forms.**Notification**(label: *string*, type?: *Types*, style?: *INotificationStyle*): *Notification*
{: .signature }

`label` string
: Specifies the label.

`type` Optional Types
: Specifies the type. (defaults to `normal` if omitted). Types:
- `normal`
- `info`
- `success`
- `warning`
- `error`

`style` Optional INotificationStyle
: Specifies the optional style.

#### Returns
`Notification`
