---
source: sections/_api_builder_forms_dropdown/blur.md
title: blur
bookmark: blur
description: Blurs the focus of the dropdown.
endpoint: method
signature: true
---

#### Signature
##### **blur**(): *void*
{: .signature }

#### Returns
Nothing
