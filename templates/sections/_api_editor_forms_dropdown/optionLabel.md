---
source: sections/_api_builder_forms_dropdown/optionLabel.md
title: optionLabel
bookmark: option-label
description: Sets or retrieves the label of the specified option or the current option if no option value is specified.
endpoint: method
signature: true
---

#### Signature
##### **optionLabel**(value?: *T*, label?: *string*): *string*
{: .signature }

`value` Optional T
: Specifies the option value.

`disabled` Optional string
: Specifies a new label for the option.

#### Returns
Returns the label.
