---
source: sections/_api_builder_forms_dropdown/value.md
title: value
bookmark: value
description: Retrieves or specifies the selected value.
endpoint: property
signature: true
---

#### Signature
##### **value**: *T | undefined*
{: .signature }

