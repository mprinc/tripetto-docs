---
source: sections/_api_builder_forms_html/update.md
title: update
bookmark: update
description: Indicates to the control something has changed and the control needs to update its dimensions.
endpoint: method
signature: true
---

#### Signature
##### **update**(): *void*
{: .signature }

#### Returns
Nothing
