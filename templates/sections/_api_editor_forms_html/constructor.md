---
source: sections/_api_builder_forms_html/constructor.md
title: constructor
bookmark: constructor
description: Creates a new HTML instance.
endpoint: constructor
signature: true
---

#### Signature
##### new Forms.**HTML**(HTML: *string*, interactive?: *boolean*, style?: *IHTMLStyle*): *HTML*
{: .signature }

`HTML` string
: Specifies the HTML code for the control.

`interactive` Optional boolean
: Specifies if the HTML code is interactive. The touch controller is disabled in that case (defaults to `false` if omitted).

`style` Optional IHTMLStyle
: Specifies the optional style.

#### Returns
`HTML`
