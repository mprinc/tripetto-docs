---
source: sections/_api_functions/isNumber.md
title: isNumber
bookmark: is-number
description: Validates if the supplied variable is a number (finite or infinite). To make sure the supplied number is a valid finite number, use `isNumberFinite`.
endpoint: function
signature: true
code: |
  ``` typescript
  isNumber(1); // Returns `true`
  isNumber(Infinity); // Returns `true`
  isNumber(NaN); // Returns `false`
  isNumber("1"); // Returns `false`
  ```
---

#### Signature
##### **isNumber**(num: *any*): *boolean*
{: .signature }

`num` any
: Variable to validate.

#### Returns
Returns `true` if the variable is a number.
