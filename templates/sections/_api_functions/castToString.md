---
source: sections/_api_functions/castToString.md
title: castToString
bookmark: cast-to-string
description: Cast a variable to a string.
endpoint: function
signature: true
code: |
  ``` typescript
  castToString(1); // Returns `1`
  castToString(1.2); // Returns `1.2`
  castToString(undefined); // Returns ``
  ```
---

#### Signature
##### **castToString**(value: *any*, default?: *string*): *string*
{: .signature }

`value` any
: Source variable.

`default` Optional string
: Optional parameter which specifies the default string if the supplied source variable cannot be casted.

#### Returns
Returns the string value.
