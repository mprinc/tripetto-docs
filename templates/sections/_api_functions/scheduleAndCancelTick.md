---
source: sections/_api_functions/scheduleAndCancelTick.md
title: scheduleAndCancelTick
bookmark: schedule-and-cancel-tick
description: Calls the supplied function asynchronous on the next tick and cancels a pending call.
endpoint: function
signature: true
---

#### Signature
##### **scheduleAndCancelTick**(callee: *function | undefined*, handle: *number*, ...arguments: *any[]*): *number*
{: .signature }

`callee` function | undefined
: Specifies the function to execute.

`handle` number
: Specifies the handle of the call to cancel.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns the call handle.
