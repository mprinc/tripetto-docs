---
source: sections/_api_functions/callBind.md
title: callBind
bookmark: call-bind
description: Calls the supplied function and binds the specified context.
endpoint: function
signature: true
---

#### Signature
##### **callBind**\<T\>(callee: *function | undefined*, context: *__type*, ...arguments: *any[]*): *T | undefined*
{: .signature }

`callee` function | undefined
: Specifies the function to execute.

`context` __type
: Context to bind to.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns `undefined` if the call fails or the return value of the function which is executed if the call succeeded.
