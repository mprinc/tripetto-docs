---
source: sections/_api_functions/scheduleFrame.md
title: scheduleFrame
bookmark: schedule-frame
description: Calls the supplied function asynchronous on the next frame.
endpoint: function
signature: true
---

#### Signature
##### **scheduleFrame**(callee: *function | undefined*, ...arguments: *any[]*): *number*
{: .signature }

`callee` function | undefined
: Specifies the function to execute.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns the call handle.
