---
source: sections/_api_functions/isNull.md
title: isNull
bookmark: is-null
description: Validates if the supplied variable is `null`.
endpoint: function
signature: true
code: |
  ``` typescript
  isNull(null); // Returns `true`
  isNull(undefined); // Returns `true`
  isNull({}); // Returns `false`
  ```
---

#### Signature
##### **isNull**(variable: *any*): *boolean*
{: .signature }

`variable` any
: Variable to validate.

#### Returns
Returns `true` if the variable is `null`.

#### Note
Please consider the use of `null`. TypeScript has two bottom types: `null` and `undefined`. They are intented to mean different things:
  - Something hasn't been initialized: `undefined`
  - Something is current unavailable: `null` Most other languages only have one (commonly called `null`). Since by default JavaScript will evaluate an uninitialized variable/parameter/property to `undefined` (you don't get a choice) we recommend you just use that for your own unavailable status and don't bother with `null`.
