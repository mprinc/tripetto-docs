---
source: sections/_api_functions/cancelInterval.md
title: cancelInterval
bookmark: cancel-interval
description: Cancels an interval call.
endpoint: function
signature: true
---

#### Signature
##### **cancelInterval**(handle: *number*): *number*
{: .signature }

`handle` number
: Contains the handle of the asynchronous interval call.

#### Returns
Always returns 0.
