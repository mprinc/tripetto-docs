---
source: sections/_api_functions/getAny.md
title: getAny
bookmark: get-any
description: Retrieves the value of the specified variable in the object array.
endpoint: function
signature: true
  ```
---

#### Signature
##### **getAny**\<T\>(object: *IObject | undefined*, name: *string*): *T | undefined*
{: .signature }

`object` IObject | undefined
: Reference to the object.

`name` string
: Name of the variable.

#### Returns
Returns the value of the variable.
