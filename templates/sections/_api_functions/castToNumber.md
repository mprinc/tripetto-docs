---
source: sections/_api_functions/castToNumber.md
title: castToNumber
bookmark: cast-to-number
description: Cast a variable to a rounded number.
endpoint: function
signature: true
code: |
  ``` typescript
  castToNumber("1"); // Returns `1`
  castToNumber("1.5"); // Returns `2`
  castToNumber(undefined); // Returns `0`
  ```
---

#### Signature
##### **castToNumber**(value: *any*, default?: *number*): *number*
{: .signature }

`value` any
: Source variable.

`default` Optional number
: Optional parameter which specifies the default number if the supplied source variable cannot be casted.

#### Returns
Returns the number value.
