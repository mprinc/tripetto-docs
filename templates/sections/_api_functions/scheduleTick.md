---
source: sections/_api_functions/scheduleTick.md
title: scheduleTick
bookmark: schedule-tick
description: Calls the supplied function asynchronous on the next tick.
endpoint: function
signature: true
---

#### Signature
##### **scheduleTick**(callee: *function | undefined*, ...arguments: *any[]*): *number*
{: .signature }

`callee` function | undefined
: Specifies the function to execute.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns the call handle.
