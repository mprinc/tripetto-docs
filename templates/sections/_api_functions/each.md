---
source: sections/_api_functions/each.md
title: each
bookmark: each
description: Iterates through an array, collection or enumerable object and invokes the supplied function for each element.
endpoint: function
signature: true
code: |
  ``` typescript
  let nResult = 0;
  each([1, 2, 3], (nItem: number) => nResult += nItem);
  // The value of `nResult` is `6`
  ```
---

#### Signature
##### **each**\<T\>(list: *TList\<T\> | undefined*, callee: *function*, options?: *object*): *TList\<T\> | undefined*
{: .signature }

`list` TList\<T\> | undefined
: Specifies the array, collection or enumerable object to iterate through.

`callee` function
: Specifies the function to be invoked for each element. The element value will be exposed to the function as the first argument of the argument list. Additional arguments can be specified and will be pushed to the function.

`options` Optional object
: Specifies the options.

#### Returns
Returns a reference to the list.
