---
source: sections/_api_functions/isArray.md
title: isArray
bookmark: is-array
description: Validates if the supplied variable is an array.
endpoint: function
signature: true
code: |
  ``` typescript
  isArray([]); // Returns `true`
  isArray([1]); // Returns `true`
  isArray(new Array()); // Returns `true`
  isArray({}); // Returns `false`
  ```
---

#### Signature
##### **isArray**(array: *any*): *boolean*
{: .signature }

`array` any
: Variable to validate.

#### Returns
Returns `true` if the variable is an array.
