---
source: sections/_api_functions/eachReverse.md
title: eachReverse
bookmark: each-reverse
description: Iterates through an array, collection or enumerable object in reverse and invokes the supplied function for each element.
endpoint: function
signature: true
code: |
  ``` typescript
  let sResult = "";
  eachReverse([1, 2, 3], (nItem: number) => nResult += CastToString(nItem));
  // The value of `sResult` is `321`
  ```
---

#### Signature
##### **eachReverse**\<T\>(list: *TList\<T\> | undefined*, callee: *function*, options?: *object*): *TList\<T\> | undefined*
{: .signature }

`list` TList\<T\> | undefined
: Specifies the array, collection or enumerable object to iterate through.

`callee` function
: Specifies the function to be invoked for each element. The element value will be exposed to the function as the first argument of the argument list. Additional arguments can be specified and will be pushed to the function.

`options` Optional object
: Specifies the options.

#### Returns
Returns a reference to the list.
