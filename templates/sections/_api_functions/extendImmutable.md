---
source: sections/_api_functions/extendImmutable.md
title: extendImmutable
bookmark: extend-immutable
description: Extend an object by appending all of the properties from each supplied object. When there are identical properties, the right-most property takes precedence. This function returns a immutable object reference which is basically a new combined copy of the supplied objects.
endpoint: function
signature: true
code: |
  ``` typescript
  extendImmutable({ a: 1, b: 2, c }, { b: 3, d: 4 }, { d: 5 }); // Returns `{ a: 1, b: 3, d: 5 }`
  ```
---

#### Signature
##### **extendImmutable**\<T\>(...objects: (*undefined | T*)[]): *T*
{: .signature }

`objects` (undefined | T)[]
: Specifies the objects to extend the immutable object with.

#### Returns
Returns the extended immutable object.
