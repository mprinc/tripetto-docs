---
source: sections/_api_functions/scheduleUIInterval.md
title: scheduleUIInterval
bookmark: schedule-ui-interval
description: Schedules the supplied function and invokes it on the specified interval (using `requestAnimationFrame`) which is optimal for UI related timers.
endpoint: function
signature: true
  ```
---

#### Signature
##### **scheduleUIInterval**(callee: *function*, interval: *number*, ...arguments: *any[]*): *number*
{: .signature }

`callee` function
: Specifies the function to execute.

`interval` number
: Specifies the interval in milliseconds.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns the call handle.
