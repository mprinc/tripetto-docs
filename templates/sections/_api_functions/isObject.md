---
source: sections/_api_functions/isObject.md
title: isObject
bookmark: is-object
description: Validates if the supplied variable is an object and is available (not `null` or `undefined`).
endpoint: function
signature: true
code: |
  ``` typescript
  isObject({}); // Returns `true`
  isObject([]); // Returns `false`
  isObject(null); // Returns `false`
  isObject(undefined); // Returns `false`
  ```
---

#### Signature
##### **isObject**(object: *any*): *boolean*
{: .signature }

`object` any
: Variable to validate.

#### Returns
Returns `true` if the variable is an object.
