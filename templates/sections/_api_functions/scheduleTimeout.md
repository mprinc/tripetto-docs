---
source: sections/_api_functions/scheduleTimeout.md
title: scheduleTimeout
bookmark: schedule-timeout
description: Schedules the supplied function and invokes it on the specified timeout (using `setTimeout`).
endpoint: function
signature: true
---

#### Signature
##### **scheduleTimeout**(callee: *function*, timeout: *number*, ...arguments: *any[]*): *number*
{: .signature }

`callee` function
: Specifies the function to execute.

`timeout` number
: Specifies the timeout in milliseconds.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns the call handle.
