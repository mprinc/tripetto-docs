---
source: sections/_api_functions/filter.md
title: filter
bookmark: filter
description: Selects all items from an array, collection or enumerable object that match the truth function.
endpoint: function
signature: true
code: |
  ``` typescript
  filter<number>([1, 2, 3], (nItem: number) => nItem > 1); // Returns `[2, 3]`
  ```
---

#### Signature
##### **filter**\<T\>(list: *TList\<T\> | undefined*, truth: *function*): *T[]*
{: .signature }

`list` TList\<T\> | undefined
: Specifies the array, collection or enumerable object to iterate through.

`truth` function
: Specifies the optional truth function. Each item is supplied to the function, passing the item as argument. The function should return a boolean value whether to include the item in the selection. If omitted all items are selected.

#### Returns
Returns an array with the selected items.
