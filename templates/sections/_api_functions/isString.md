---
source: sections/_api_functions/isString.md
title: isString
bookmark: is-string
description: Validates if the supplied variable is a string.
endpoint: function
signature: true
code: |
  ``` typescript
  isString("1"); // Returns `true`
  isString(1); // Returns `false`
  ```
---

#### Signature
##### **isString**(str: *any*): *boolean*
{: .signature }

`str` any
: Variable to validate.

#### Returns
Returns `true` if the variable is a string.
