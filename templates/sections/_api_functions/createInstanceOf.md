---
source: sections/_api_functions/createInstanceOf.md
title: createInstanceOf
bookmark: create-instance-of
description: Creates a new class instance with the supplied constructor and arguments array.
endpoint: function
signature: true
code: |
  ``` typescript
  class Example {
      constructor(public a: string, public b: number) {}
  }

  let pExample = createInstanceOf(Example, "Hello", 1);
  ```
---

#### Signature
##### **createInstanceOf**\<T\>(constructor: *object*, ...arguments: *any[]*): *T*
{: .signature }

`constructor` object
: Specifies the constructor.

`...arguments` any[]
: Optional array with arguments which will be passed to the constructor.

#### Returns
Returns the instance.
