---
source: sections/_api_functions/isFloat.md
title: isFloat
bookmark: is-float
description: Validates if the supplied variable is a floating point number and optionally checks if the number is within the specified range.
endpoint: function
signature: true
code: |
  ``` typescript
  isFloat(1.01); // Returns `true`
  isFloat(1); // Returns `false`
  isFloat(Infinity); // Returns `false`
  isFloat(NaN); // Returns `false`
  isFloat("1"); // Returns `false`
  ```
---

#### Signature
##### **isFloat**(float: *any*, rangeLower?: *number*, rangeUpper?: *number*): *boolean*
{: .signature }

`float` any
: Variable to validate.

`rangeLower` Optional number
: Optional parameter which specifies the lower range.

`rangeUpper` Optional number
: Optional parameter which specifies the upper range.

#### Returns
Returns `true` if the variable is a floating point number.
