---
source: sections/_api_functions/destroy.md
title: destroy
bookmark: destroy
description: Destroys the specified variable in the object array.
endpoint: function
signature: true
---

#### Signature
##### **destroy**(object: *IObject | undefined*, name: *string*): *boolean*
{: .signature }

`object` IObject | undefined
: Reference to the object.

`name` string
: Name of the variable.

#### Returns
Returns `true` if the variable is deleted.
