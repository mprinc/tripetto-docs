---
source: sections/_api_functions/hasOneOrMore.md
title: hasOneOrMore
bookmark: has-one-or-more
description: Verifies some items of an array, collection or enumerable object against the truth function.
endpoint: function
signature: true
code: |
  ``` typescript
  hasOneOrMore<number>([1, 2, 3], (nItem: number) => nItem > 2); // Returns `true`
  hasOneOrMore<number>([1, 2, 3], (nItem: number) => nItem > 3); // Returns `false`
  ```
---

#### Signature
##### **hasOneOrMore**\<T\>(list: *TList\<T\> | undefined*, truth: *function*): *boolean*
{: .signature }

`list` TList\<T\> | undefined
: Specifies the array, collection or enumerable object to iterate through.

`truth` function
: Specifies the truth function. Each item is supplied to the function, passing the item as argument. The function should return a boolean value with the individual verification result.

#### Returns
Returns `true` when one or more items pass the verification test.
