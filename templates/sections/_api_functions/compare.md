---
source: sections/_api_functions/compare.md
title: compare
bookmark: compare
description: Does a type or value comparison of two variables or a structure.
endpoint: function
signature: true
code: |
  ``` typescript
  compare({ a: 1, b: 2 }, { a: 1, b: 3 }, false); // Returns `true`
  compare({ a: 1, b: 2 }, { a: 1, c: 3 }, false); // Returns `false`
  compare({ a: 1, b: 2 }, { a: 1, b: 3 }, true); // Returns `false`
  compare({ a: 1, b: 2 }, { a: 1, b: 2 }, true); // Returns `true`
  ```
---

#### Signature
##### **compare**(a: *any*, b: *any*, compareValues: *boolean*): *boolean*
{: .signature }

`a` any
: Specifies the first variable.

`b` any
: Specifies the second  variable.

`compareValues` boolean
: Specifies if the values or just the structure needs to be compared.

#### Returns
Returns `true` if the objects are identical.
