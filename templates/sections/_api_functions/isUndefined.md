---
source: sections/_api_functions/isUndefined.md
title: isUndefined
bookmark: is-undefined
description: Validates if the supplied variable is undefined.
endpoint: function
signature: true
code: |
  ``` typescript
  isUndefined(1); // Returns `false`
  isUndefined(); // Returns `true`
  ```
---

#### Signature
##### **isUndefined**(variable: *any*): *boolean*
{: .signature }

`variable` any
: Variable to validate.

#### Returns
Returns `true` if the variable is undefined.
