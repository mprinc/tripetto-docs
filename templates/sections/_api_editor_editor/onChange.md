---
source: sections/_api_builder_builder/onChange.md
title: onChange
bookmark: on-change
description: Invoked when the definition is changed.
endpoint: method
signature: true
---

#### Signature
##### **onChange**(): *(pDefinition: IDefinition, pBuilder: Builder) => void*
{: .signature }
