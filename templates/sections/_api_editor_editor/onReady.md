---
source: sections/_api_builder_builder/onReady.md
title: onReady
bookmark: on-ready
description: Invoked when the builder is ready to use.
endpoint: method
signature: true
---

#### Signature
##### **onReady**(): *(pBuilder: Builder) => void*
{: .signature }
