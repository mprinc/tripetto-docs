---
source: sections/_api_builder_builder/resize.md
title: resize
bookmark: resize
description: Notify the builder about a parent element resize.
endpoint: method
signature: true
---

#### Signature
##### **resize**(): *this*
