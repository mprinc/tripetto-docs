---
source: sections/_api_builder_builder/name.md
title: name
bookmark: name
description: Retrieves the name of the definition.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **name**(): *string*
