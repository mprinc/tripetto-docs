---
source: sections/_api_builder_builder/clear.md
title: clear
bookmark: clear
description: Clears the form (creates a new empty one).
endpoint: method
signature: true
---

#### Signature
##### **clear**(): *this*
