---
source: sections/_api_class_callback/cancel.md
title: cancel
bookmark: cancel
description: Cancels the callback loop.
endpoint: method
signature: true
---

#### Signature
##### **cancel**(): *boolean*
{: .signature }

#### Returns
Returns `true` if the callback loop is terminated.
