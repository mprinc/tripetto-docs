---
source: sections/_api_class_callback/constructor.md
title: constructor
bookmark: constructor
description: Creates a new callback.
endpoint: constructor
signature: true
---

#### Signature
##### new **Callback**(properties: *ICallbackProperties\<T\>*): *Callback*
{: .signature }

`properties` ICallbackProperties\<T\>
: Specifies the callback properties.

#### Returns
A new `Callback`.
