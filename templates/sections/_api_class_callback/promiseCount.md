---
source: sections/_api_class_callback/promiseCount.md
title: promiseCount
bookmark: promise-count
description: Retrieves the promise invoke count.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **promiseCount**: *number*
{: .signature }
