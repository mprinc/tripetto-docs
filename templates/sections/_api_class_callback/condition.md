---
source: sections/_api_class_callback/condition.md
title: condition
bookmark: condition
description: Retrieves or specifies the callback condition state.
endpoint: property
signature: true
---

#### Signature
##### **condition**: *boolean*
{: .signature }
