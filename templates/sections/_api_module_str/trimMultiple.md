---
source: sections/_api_module_str/trimMultiple.md
title: trimMultiple
bookmark: trim-multiple
description: Trims a string by removing all multiple whitespaces (`_ab__cd_` -> `_ab_cd_`).
endpoint: function
signature: true
code: |
  ``` typescript
  trimMultiple(" ab  cd "); // Returns ` ab cd `
  ```
---

#### Signature
##### Str.**trimMultiple**(value: *string*): *string*
{: .signature }

`value` string
: Source string.

#### Returns
Returns the trimmed string.
