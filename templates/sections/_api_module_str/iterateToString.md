---
source: sections/_api_module_str/iterateToString.md
title: iterateToString
bookmark: iterate-to-string
description: Converts an array or object list to a concatenated string.
endpoint: function
signature: true
code: |
  ``` typescript
  iterateToString(["a", "b", "c"]); // Returns `abc`
  iterateToString(["a", "b", "c"], "-"); // Returns `a-b-c`
  iterateToString(["a", "b", "c"], "/", (sValue: string) => sValue + sValue); // Returns `aa/bb/cc`
  ```
---

#### Signature
##### Str.**iterateToString**\<T\>(list: *TList\<T\>*, separator?: *string*, cast?: *function*, ...arguments: *any[]*): *string*
{: .signature }

`list` TList\<T\>
: Specifies the array, collection or enumerable object to iterate through.

`separator` Optional string
: Optional parameter which specifies the separator string or char (defaults to `""` if omitted).

`cast` Optional function
: Optional function to be invoked for each element instead of automatic casting to strings. The element value will be exposed to the function as the first argument of the argument list. Additional arguments can be specified and will be pushed to the function.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns the concatenated string.
