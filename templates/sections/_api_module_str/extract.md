---
source: sections/_api_module_str/extract.md
title: extract
bookmark: extract
description: Extracts the part of the string between the first occurrence of `left` and the optional occurrence of `right`.
endpoint: function
signature: true
code: |
  ``` typescript
  extract("Lorem ipsum dolor", { left: "Lo", right: "m" }); // Returns `re`
  extract("Lorem ipsum dolor", { left: "Lo", right: "m", fromEnd: true }); // Returns `rem ipsu`
  ```
---

#### Signature
##### Str.**extract**(value: *string*, options: *{ ... }*): *string*
{: .signature }

`value` string
: Specifies the string to be converted (variable will be casted to a string if necessary).

`options` object
: Specifies the extract options:
- `left` (string): Left string selector;
- `right` (string): Right string selector;
- `fromEnd` (optional boolean): Specifies the search direction of the right string selector. If `false` is specified, the function searches for the first occurrence of `Right` directly after the position of `Left`.
- `ignoreCase` (optional boolean): Specifies if a case insensitive check should be performed.

#### Returns
Returns the string between `left` and `right` or an empty string if an error occurs.
