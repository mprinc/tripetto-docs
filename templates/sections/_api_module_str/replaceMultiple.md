---
source: sections/_api_module_str/replaceMultiple.md
title: replaceMultiple
bookmark: replace-multiple
description: Replaces all occurrences of `what` with `with` in the specified string.
endpoint: function
signature: true
code: |
  ``` typescript
  replaceMultiple("Hello", ["ll", "o"], "-"); // Returns `He---`
  ```
---

#### Signature
##### Str.**replaceMultiple**(value: *string*, what: *string[]*, with?: *string*, ignoreCase?: *boolean*): *string*
{: .signature }

`value` string
: Specifies the input string (variable will be casted to a string if necessary).

`what` string[]
: Specifies the strings to search for.

`with` Optional string
: Specifies the replace string. If omitted an empty string will be used.

`ignoreCase` Optional boolean
: Specifies if the string replace should be case insensitive. (defaults to `false` if omitted).

#### Returns
Returns the replaced string.
