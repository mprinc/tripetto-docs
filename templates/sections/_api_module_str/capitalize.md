---
source: sections/_api_module_str/capitalize.md
title: capitalize
bookmark: capitalize
description: Capitalizes the first character of a string or the first character of each word.
endpoint: function
signature: true
code: |
  ``` typescript
  capitalize("lorem ipsum. dolor"); // Returns `Lorem ipsum. dolor`
  capitalize("lorem ipsum. dolor", "each-word"); // Returns `Lorem Ipsum. Dolor`
  capitalize("lorem ipsum. dolor", "each-sentence"); // Returns `Lorem ipsum. Dolor`
  ```
---

#### Signature
##### Str.**capitalize**(value: *string*, mode?: *"first-character" | "each-word" | "each-sentence"*, lowercase?: *boolean*): *string*
{: .signature }

`value` string
: Source string.

`mode` Optional "first-character" | "each-word" | "each-sentence"
: Specifies the mode: `first-character`, `each-word` or `each-sentence` (defaults to `first-character` if omitted).

`lowercase` Optional boolean
: Converts the string to lowercase before capitalizing it (defaults to `false` if omitted).

#### Returns
Returns the capitalized string.
