---
source: sections/_api_module_str/CRLFToHTML.md
title: CRLFToHTML
bookmark: crlf-to-html
description: Converts carriage returns and/or newlines to HTML breaks.
endpoint: function
signature: true
code: |
  ``` typescript
  CRLFToHTML("Row 1\r\nRow 2"); // Returns `Row 1<br />Row 2`
  ```
---

#### Signature
##### Str.**CRLFToHTML**(value: *string*): *string*
{: .signature }

`value` string
: Specifies the string to be converted (variable will be casted to a string if necessary).

#### Returns
Returns the converted string.
