---
source: sections/_api_module_str/trimRight.md
title: trimRight
bookmark: trim-right
description: Trims a string at the right by removing all trailing whitespaces (`_ab__cd_` -> `_ab__cd`).
endpoint: function
signature: true
code: |
  ``` typescript
  trimRight(" ab  cd "); // Returns ` ab  cd`
  ```
---

#### Signature
##### Str.**trimRight**(value: *string*): *string*
{: .signature }

`value` string
: Source string.

#### Returns
Returns the trimmed string.
