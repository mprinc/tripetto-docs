---
source: sections/_api_module_slots_string/toValue.md
title: toValue
bookmark: to-value
description: Converts the supplied data to a valid string value.
endpoint: method
signature: true
---

#### Signature
##### **toValue**(data: *TSerializeTypes*): *string*
{: .signature }

`data` TSerializeTypes
: Specifies the data.

#### Returns
Returns the string value.
