---
source: sections/_api_builder_forms_datetime/showHourSelector.md
title: showHourSelector
bookmark: show_hour_selector
description: Shows the hour selector.
endpoint: method
signature: true
---

#### Signature
##### **showHourSelector**(): *void*
{: .signature }

#### Returns
Nothing
