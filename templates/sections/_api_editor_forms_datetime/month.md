---
source: sections/_api_builder_forms_datetime/month.md
title: month
bookmark: month
description: Retrieves or specifies the month.
endpoint: property
signature: true
---

#### Signature
##### **month**: *TMonths*
{: .signature }
