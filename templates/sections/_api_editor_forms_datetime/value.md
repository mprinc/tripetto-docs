---
source: sections/_api_builder_forms_datetime/value.md
title: value
bookmark: value
description: Retrieves or specifies the selected value.
endpoint: property
signature: true
---

#### Signature
##### **value**: *number | undefined*
{: .signature }

