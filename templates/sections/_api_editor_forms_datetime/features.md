---
source: sections/_api_builder_forms_datetime/features.md
title: features
bookmark: features
description: Specifies the date control features which should be enabled.
endpoint: method
signature: true
---

#### Signature
##### **features**(features: *DateTimeFeatures*): *this*
{: .signature }

`features` DateTimeFeatures
: Specifies the features which should be enabled.

#### Returns
Returns a reference to the control to allow chaining.
