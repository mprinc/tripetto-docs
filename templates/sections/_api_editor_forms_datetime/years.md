---
source: sections/_api_builder_forms_datetime/years.md
title: years
bookmark: years
description: Specifies the years range.
endpoint: method
signature: true
---

#### Signature
##### **years**(from: *number*, to: *number*): *this*
{: .signature }

`from` number
: Specifies the from year.

`to` number
: Specifies the to year.

#### Returns
Returns a reference to the control to allow chaining.
