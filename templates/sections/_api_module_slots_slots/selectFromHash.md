---
source: sections/_api_module_slots_slots/selectFromHash.md
title: selectFromHash
bookmark: select-from-hash
description: Selects a slot (or slot group) with the specified hash.
endpoint: method
signature: true
---

#### Signature
##### **selectFromHash**\<T\>(hash: *string*, parent?: *string*): *T | undefined*
{: .signature }

`hash` string
: Specifies the hash to search for.

`parent` Optional string
: Specifies the parent identifier or hash of a slot group (defaults to `""` if omitted).

#### Returns
Returns the slot or `undefined` if no slot is found.
