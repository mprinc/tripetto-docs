---
source: sections/_api_module_slots_slots/delete.md
title: delete
bookmark: delete
description: Deletes a slot.
endpoint: method
signature: true
---

#### Signature
##### **delete**\<T\>(referenceOrHash: *string*, kind?: *Kinds*): *T | undefined*
{: .signature }

`referenceOrHash` string
: Reference or hash of the slot.

`kind` Optional Kinds
: Specifies the optional kind of the slot.

#### Returns
Returns the slot or `undefined` if no slot is found.
