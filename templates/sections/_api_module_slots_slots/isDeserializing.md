---
source: sections/_api_module_slots_slots/isDeserializing.md
title: isDeserializing
bookmark: is-deserializing
description: Retrieves if the slots are being deserialized.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **isDeserializing**(): *boolean*
{: .signature }
