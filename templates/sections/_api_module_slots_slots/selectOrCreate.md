---
source: sections/_api_module_slots_slots/selectOrCreate.md
title: selectOrCreate
bookmark: select-or-create
description: Selects or creates a slot.
endpoint: method
signature: true
---

#### Signature
##### **selectOrCreate**\<T\>(kind: *Kinds*, type: *ISlotType\<T\>*, reference: *string*, sequence?: *number*): *T*
{: .signature }

`kind` Kinds
: Specifies the kind of the slot.

`type` ISlotType\<T\>
: Specifies the slot type.

`reference` string
: Specifies the reference.

`sequence` Optional number
: Specifies the sequence number (defaults to `0` if omitted).

#### Returns
Returns the slot.
