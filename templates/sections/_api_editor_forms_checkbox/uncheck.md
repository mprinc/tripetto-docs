---
source: sections/_api_builder_forms_checkbox/uncheck.md
title: uncheck
bookmark: uncheck
description: Unchecks the checkbox.
endpoint: method
signature: true
---

#### Signature
##### **uncheck**(): *this*
{: .signature }

#### Returns
Returns a reference to the control to allow chaining.
