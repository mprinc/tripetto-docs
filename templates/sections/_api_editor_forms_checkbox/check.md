---
source: sections/_api_builder_forms_checkbox/check.md
title: check
bookmark: check
description: Checks the checkbox.
endpoint: method
signature: true
---

#### Signature
##### **check**(): *this*
{: .signature }

#### Returns
Returns a reference to the control to allow chaining.
