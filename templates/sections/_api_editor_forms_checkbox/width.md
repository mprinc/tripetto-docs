---
source: sections/_api_builder_forms_checkbox/width.md
title: width
bookmark: width
description: Sets the width of the control.
endpoint: method
signature: true
---

#### Signature
##### **width**(width: *number*): *this*
{: .signature }

`width` number
: Specifies the control width in pixels.

#### Returns
Returns a reference to the control to allow chaining.
