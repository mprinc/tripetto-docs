---
source: sections/_api_builder_forms_checkbox/checked.md
title: checked
bookmark: checked
description: Sets the checked state of the checkbox.
endpoint: method
signature: true
---

#### Signature
##### **checked**(checked: *boolean*): *this*
{: .signature }

`checked` boolean
: Specifies the state (defaults to `true` if omitted).

#### Returns
Returns a reference to the control to allow chaining.
