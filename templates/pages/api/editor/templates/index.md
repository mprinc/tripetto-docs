---
base: ../../../
permalink: /api/builder/templates/
title: Templates / Builder - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_builder_templates | sort: "path" %}

<section>
    <section>
        <h2>
            Templates
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">Lorem ipsum.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
