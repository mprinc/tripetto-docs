---
base: ../../../
permalink: /api/builder/interface-builder-properties
source: pages/api/builder/interface-builder-properties
title: IBuilderProperties / Builder - API - Tripetto Documentation
heading: IBuilderProperties
---

Specifies the properties for a new `Builder`-instance.

`element` Optional HTMLElement
: Host HTML element (the builder is displayed inside this element or the document body if omitted).

`fonts` Optional string
: Specifies the path to the folder with the font files. This can be a relative path or an absolute URL.

`style` Optional IBuilderStyle
: Contains the builder styles (can be used to create a custom skin).  **You need a commercial license for this.**

`locale` Optional ILocale
: Specifies the locale.

`translations` Optional ITranslation | ITranslation[]
: Optional translations for the builder and the blocks.

`zoom` Optional "1:1" | "fit" | "fit-horizontal" | "fit-vertical"
: Specifies the initial zoom state (defaults to `fit`).

`disableZoombar` Optional boolean
: Disables the zoombar.

`disableLogo` Optional boolean
: Disables the Tripetto logo in the navigation bar.  **You need a commercial license for this.**

`disableSaveButton` Optional boolean
: Disables the save button in the navigation bar.

`disableRestoreButton` Optional boolean
: Disables the restore button in the navigation bar (`true` by default).

`disableClearButton` Optional boolean
: Disables the clear button in the navigation bar (`true` by default).

`disableEditButton` Optional boolean
: Disables the edit button in the navigation bar.

`disableCloseButton` Optional boolean
: Disables the close button in the navigation bar.

`disableTutorialButton` Optional boolean
: Disables the tutorial button in the navigation bar.

`disableGlobalCSSRules` Optional boolean
: Specifies if the global CSS rules should not be added to the DOM.

`disableOpenCloseAnimation` Optional boolean
: Disables the open and close animation of the builder layer.

`showTutorial` Optional boolean
: Shows the tutorial dialog on startup.

`previewURL` Optional string
: Specifies the URL of a preview page.

`supportURL` Optional string | false
: Specifies an URL to custom a help or support page (supply `false` to disable this button).

`onLoad` (builder: Builder) => void
: Invoked when the builder is loaded.

`onReady` (builder: Builder) => void
: Invoked when the builder is ready to use.

`onOpen` (builder: Builder) => void
: Invoked when the builder is opened.

`onSave` (definition: IDefinition, builder: Builder) => void
: Invoked when the user clicks the save button (or when the `Save()` command is called).

`onChange` (definition: IDefinition, builder: Builder) => void
: Invoked when a change occurs.

`onClose` (pBuilder: Builder) => void
: Invoked when the builder is closed.
