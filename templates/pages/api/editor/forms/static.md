---
base: ../../../../
permalink: /api/builder/forms/static/
title: Static / Forms / Builder - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_builder_forms_static | sort: "path" %}

<section>
    <section>
        <h2>
            Static
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">This class implements the form static control which can be used to add static text to forms.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
