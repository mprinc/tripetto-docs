---
base: ../../../../
permalink: /api/builder/forms/email/
title: Email / Forms / Builder - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_builder_forms_email | sort: "path" %}

<section>
    <section>
        <h2>
            Email
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">This class implements an email form control.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
