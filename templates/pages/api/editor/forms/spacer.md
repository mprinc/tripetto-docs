---
base: ../../../../
permalink: /api/builder/forms/spacer/
title: Spacer / Forms / Builder - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_builder_forms_spacer | sort: "path" %}

<section>
    <section>
        <h2>
            Spacer
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">This class implements the form spacer control which can be used to add whitelines (space) between controls.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
