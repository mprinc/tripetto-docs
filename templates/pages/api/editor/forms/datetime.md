---
base: ../../../../
permalink: /api/builder/forms/datetime/
title: DateTime / Forms / Builder - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_builder_forms_datetime | sort: "path" %}

<section>
    <section>
        <h2>
            DateTime
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">This class implements the form date/time control.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
