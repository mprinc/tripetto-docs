---
---

#### Signature
##### **onNameChange**(item: *T*): *void*
{: .signature }

#### Description
Invoked when the name of an item is changed.

`item` T
: Reference to the item which name is changed.
