[![pipeline status](https://gitlab.com/tripetto/docs/badges/master/pipeline.svg)](https://gitlab.com/tripetto/docs/commits/master)

Tripetto technical documentation website published at [docs.tripetto.com](https://docs.tripetto.com). Generated using [Jekyll](https://jekyllrb.com/).

## Prerequisites
Make sure [Ruby](https://www.ruby-lang.org/en/) is installed. Run the following command to install the required gems.

```bash
gem install jekyll bundler
```

## Local build
To write documentation and test the website on your local machine run the following command inside the repo directory:

```bash
bundle install
bundle exec jekyll serve
```

## Deployment build
For deployment run:

```bash
bundle install
bundle exec jekyll build
```

Make sure you set the following environment variable: `JEKYLL_ENV=production`

The website is generated in the folder `./public`.

## Section/page variables
The following variables are supported in the header of a page or section:
- `title`: Title of the page or section;
- `name`: Name of the page or section in the menu;
- `bookmark`: Bookmark (`#`) of the page or section;
- `source`: Specifies the path to the source of the file;
- `code`: Example code displayed on the right;
- `sub`: Specifies if the section is a sub section;
- `signature`: Specifies if the section is API signature;
- `endpoint`: Specigies the type of endpoint, can be `put`, `delete`, `get` or `post`.

And the following variables are only available for pages:
- `base`: Specifies the relative path to the base;
- `permalink`: Specifies the path of the page itself;
- `custom`: Specifies if the page should not be rendered as a section;
- `heading`: Specifies the heading (alternate title) for pages;
- `sitemap`: Specifies if a page should be included in the sitemap (defaults to `true`).
