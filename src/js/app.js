// Contains the document title.
var docTitle = document.title;

// Contains the document scroller.
var $docScroller;

// Contains if the doc is currently scrolling.
var isDocScrolling = false;

/**
 * Finds the active hash based on the scroll position.
 */
function findHash() {
    if (!$docScroller) {
        return;
    }

    var host = window.location.href.indexOf("#") === -1 ? window.location.href + "#" : window.location.href.substr(0, window.location.href.indexOf("#") + 1);
    var top = $docScroller.scrollTop();
    var height = $docScroller.height();

    var $links = $("nav a").filter(function () {
        return this.href && (this.href + "#").indexOf(host) === 0;
    });

    var getHash = function (i) {
        var $link = $links.eq(i);
        var hash = $link.attr("href");

        if (hash) {
            hash = hash.indexOf("#") !== -1 ? hash.substr(hash.indexOf("#")) : "#";

            var $hash = $(hash === "#" ? "#_first" : hash);

            if ($hash.length > 0) {
                return $hash;
            }
        }

        return undefined;;
    }

    if (top > 72) {
        if (top > $docScroller[0].scrollHeight - height - 72) {
            return $links.last();
        }

        for (var i = $links.length - 1; i >= 0; i--) {
            var $hash = getHash(i);

            if ($hash) {
                var offset = $hash.offset().top - $docScroller.offset().top;

                if (offset < height / 2) {
                    var $return = $links.eq(i);

                    for (var j = i; j >= 0; j--) {
                        $hash = getHash(j);

                        if ($hash) {
                            offset = $hash.offset().top - $docScroller.offset().top;

                            if (offset < 0) {
                                break;
                            }

                            $return = $links.eq(j);
                        }
                    }

                    return $return;
                }
            }
        }
    }

    return $links.first();
}

/**
 * Sets the active navigation link.
 * @param {$} active Reference to the active navigation link.
 * @param {boolean} noHashUpdate Specifies if the location hash should be updated.
 */
function navigateSet($active, noLocationUpdate) {
    if (!$active) {
        return;
    }

    var name = $active.text();

    $("nav a").removeClass("active");
    $active.addClass("active");

    if (name !== "") {
        var prefix = $.trim($active.text() + " -").replace(/\s{2,}/g, " ");

        document.title = (docTitle.indexOf(prefix) === 0 ? "" : prefix + " ") + docTitle;
    }
}


/**
 * Finds the active navigation link.
 */
function navigateHighlight(scroll) {
    if (isDocScrolling && scroll) {
        return;
    }

    navigateSet(findHash());
}

/**
 * Navigates to a hash.
 * @param {string} hash Specifies the hash to navigate to.
 * @param {boolean} onload Specifies if the hash is set on load.
 */
function navigateToHash(hash, onload, fnCallback) {
    if (hash === "") {
        return false;
    }

    if (hash === "#") {
        hash = "#_first";
    }

    if (hash !== "") {
        var $hash = $(hash);

        if ($hash.length > 0) {
            var offset = parseInt($hash.offset().top - 31 - $docScroller.offset().top);

            if (!onload) {
                isDocScrolling = true;

                if (hash !== "#_first" && $hash.length > 0) {
                    var id = $hash.attr("id");

                    $hash.attr("id", "");
                    window.location.hash = hash;
                    $hash.attr("id", id);
                } else {
                    window.location.hash = "";
                }

                $docScroller.animate({
                    scrollTop: $docScroller.scrollTop() + offset
                }, 400, "swing", function () {
                    requestAnimationFrame(function () {
                        isDocScrolling = false;

                        if (fnCallback) {
                            fnCallback();
                        }
                    });
                });
            } else {
                $docScroller.scrollTop($docScroller.scrollTop() + offset);
            }

            return true;
        }
    }

    return false;
}

/**
 * Invoked when a navigation link is clicked.
 * @param {MouseEvent} event Reference to the clicked element.
 */
function navigateTo(link) {
    if (!$docScroller) {
        return;
    }

    const $link = $(link);

    var host = window.location.href.indexOf("#") === -1 ? window.location.href + "#" : window.location.href.substr(0, window.location.href.indexOf("#") + 1);
    var href = $link.attr("href");

    $docScroller.stop();

    if (href && link.href && (link.href + "#").indexOf(host) === 0) {
        if (navigateToHash(href.indexOf("#") !== -1 ? href.substr(href.indexOf("#")) : "#")) {
            navigateSet($link);

            if (document.body.classList.contains("menu")) {
                document.body.classList.toggle("menu");
            }

            return true;
        }
    }

    return false;
}

function toggleSearchBar() {
    var menu = $("#header-menu");
    var form = $("#search");
    var input = $("#search-input");
    var mobileLogo = $("#mobile-logo");

    if (menu && form && input && mobileLogo) {
        if (input.css("width") === "0px") {
            menu.css("opacity", "0");
            mobileLogo.css("opacity", "0");
            form.css("height", "64px");
            input.css("width", "100%");
            input.select();
        } else {
            menu.css("opacity", "1");
            mobileLogo.css("opacity", "1");
            form.css("height", "0px");
            input.css("width", "0px");
        }
    }
}

jQuery(function () {
    /**
     * Find our scroll element.
     */
    $docScroller = $("body > div");

    /**
     * Catch scroll events.
     */
    /*
    $docScroller.on("scroll", function () {
        navigateHighlight(true);
    });
    */

    /**
     * Catch navigation clicks.
     */
    /*
    $("nav a").click(function (event) {
        if (navigateTo(event.target)) {
            event.preventDefault();
        }
    });
    */

    /**
     * Catch bookmark clicks.
     */
    /*
    $("body div section a").filter(function () {
        if (typeof this.hash === "string" && this.hash !== "") {
            var base = this.baseURI || document.baseURI || location.href;

            return  this.href.indexOf((base.indexOf("#") === -1 ? base : base.substr(0, base.indexOf("#"))) + "#") === 0;
        }

        return false;
    }).click(function (event) {
        console.log(event.target.hash);
        if (navigateToHash(event.target.hash)) {
            event.preventDefault();
        }
    });
    */

    /**
     * Parse language buttons for code views
     */
    $("section > div.highlighter-rouge:first-of-type").each(function (i) {
        var $this = $(this).before("<ul class=\"languages\"></ul>");
        var $languages = $this.prev();
        var $notFirst = $this.nextUntil(":not(div.highlighter-rouge)");
        var $all = $this.add($notFirst);

        if ($all.length > 1) {
            $all.add($languages).wrapAll("<div class=\"code-viewer\"></div>");

            $all.each(function () {
                var title = $(this).attr("title");

                if (title) {
                    $languages.append("<li><a href=\"#\">" + title + "</a></li>");
                }
            });

            $this.css("display", "block");
            $notFirst.css("display", "none");

            $languages.find("a").first().addClass("active");

            $languages.find("a").click(function () {
                $all.css("display", "none");
                $all.eq($(this).parent().index()).css("display", "block");

                $languages.find("a").removeClass("active");
                $(this).addClass("active");

                return false;
            });

            if ($languages.children().length === 0) {
                $languages.remove();
            }
        }
    });

    /**
     * Highlight the active link.
     */
    /*
    navigateHighlight();
    */

    /** Load fonts. */
    WebFont.load({
        custom: {
            families: ["Roboto"],
            urls: ["/css/fonts.css"]
        },
        active: function () {
            $("html").removeClass("loading");
        },
        inactive: function () {
            $("html").removeClass("loading");
        }
    });
});
