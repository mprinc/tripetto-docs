---
source: sections/_guide_builder/04.07-demo.md
bookmark: library-demo
name: Demo
title: Live demo
sub: true
---

<p data-height="600" data-theme-id="dark" data-slug-hash="GxXpYG" data-default-tab="result" data-user="tripetto" data-embed-version="2" data-pen-title="Tripetto builder with some blocks" class="codepen">See the Pen <a href="https://codepen.io/tripetto/pen/GxXpYG/">Tripetto builder with some blocks</a> by Tripetto (<a href="https://codepen.io/tripetto">@tripetto</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>
