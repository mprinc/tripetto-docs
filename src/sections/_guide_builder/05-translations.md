---
source: sections/_guide_builder/005-translations.md
bookmark: translations
title: Translations
---

Currently the following translations are available:

| Locale | Language        |
|--------|-----------------|
| `en`   | English
| `nl`   | Dutch

If you want to help us translating the Tripetto builder to other languages, you're very welcome to! Download our [POT](https://unpkg.com/{{ site.npm_packages.builder }}/translations/template.pot){:target="_blank"} file and start translating it. We use [POEdit](https://poedit.net/){:target="_blank"} for this job, but you can use any other tool or service you like. Send us the translated PO file and we are more than happy to include it.

**Make sure to include your name so we can give you the appropriate credits!**

[Download POT file](https://unpkg.com/{{ site.npm_packages.builder }}/translations/template.pot){:target="_blank"}
{: .start }
