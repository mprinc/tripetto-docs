---
source: sections/_examples/02-blocks.md
title: Blocks examples
bookmark: blocks
---

#### Block boilerplate for creating new blocks
[gitlab.com/{{ site.accounts.gitlab }}/blocks/boilerplate](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/boilerplate){:target="_blank"}
{: .example }

---

#### Checkbox
Checkbox block.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/checkbox](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/checkbox){:target="_blank"}
{: .hyperlink }

---

#### Checkboxes
List of checkboxes.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/checkboxes](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/checkboxes){:target="_blank"}
{: .hyperlink }

---

#### Date
Date/time block.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/date](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/date){:target="_blank"}
{: .hyperlink }

---

#### Device
Device condition block.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/device](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/device){:target="_blank"}
{: .hyperlink }

---

#### Dropdown
Dropdown block.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/dropdown](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/dropdown){:target="_blank"}
{: .hyperlink }

---

#### E-mail
E-mail block.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/email](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/email){:target="_blank"}
{: .hyperlink }

---

#### Error
Raise error block.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/error](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/error){:target="_blank"}
{: .hyperlink }

---

#### Evaluate
Evaluate condition block.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/evaluate](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/evaluate){:target="_blank"}
{: .hyperlink }

---

#### File upload
File upload block.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/file-upload](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/file-upload){:target="_blank"}
{: .hyperlink }

---

#### Hidden field
Hidden field block.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/hidden-field](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/hidden-field){:target="_blank"}
{: .hyperlink }

---

#### Mailer
Mailer block.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/mailer](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/mailer){:target="_blank"}
{: .hyperlink }

---

#### Matrix
Matrix block.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/matrix](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/matrix){:target="_blank"}
{: .hyperlink }

---

#### Multiple choice
Multiple choice block.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/multiple-choice](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/multiple-choice){:target="_blank"}
{: .hyperlink }

---

#### Number
Standard number block

[gitlab.com/{{ site.accounts.gitlab }}/blocks/number](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/number){:target="_blank"}
{: .hyperlink }

---

#### Paragraph
Paragraph block

[gitlab.com/{{ site.accounts.gitlab }}/blocks/paragraph](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/paragraph){:target="_blank"}
{: .hyperlink }

---

#### Password
Password block

[gitlab.com/{{ site.accounts.gitlab }}/blocks/password](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/password){:target="_blank"}
{: .hyperlink }

---

#### Phone number
Phone number block

[gitlab.com/{{ site.accounts.gitlab }}/blocks/phone-number](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/phone-number){:target="_blank"}
{: .hyperlink }

---

#### Radio buttons
Radio button block

[gitlab.com/{{ site.accounts.gitlab }}/blocks/radiobuttons](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/radiobuttons){:target="_blank"}
{: .hyperlink }

---

#### Rating
Rating block

[gitlab.com/{{ site.accounts.gitlab }}/blocks/rating](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/rating){:target="_blank"}
{: .hyperlink }

---

#### Regex
Regular expression condition block

[gitlab.com/{{ site.accounts.gitlab }}/blocks/regex](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/regex){:target="_blank"}
{: .hyperlink }

---

#### Statement
Statement block

[gitlab.com/{{ site.accounts.gitlab }}/blocks/statement](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/statement){:target="_blank"}
{: .hyperlink }

---

#### Text
Text block

[gitlab.com/{{ site.accounts.gitlab }}/blocks/text](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/text){:target="_blank"}
{: .hyperlink }

---

#### Textarea
Textarea block.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/textarea](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/textarea){:target="_blank"}
{: .hyperlink }

---

#### URL
URL block.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/url](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/url){:target="_blank"}
{: .hyperlink }

---

#### Yes/no
Yes/no block.

[gitlab.com/{{ site.accounts.gitlab }}/blocks/yes-no](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/yes-no){:target="_blank"}
{: .hyperlink }
