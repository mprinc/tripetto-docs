---
source: sections/_quickstart/02-concepts.md
bookmark: concepts
title: Concepts
---

### Builder
#### Showing you the flow
You use the graphical builder to create and edit smart forms with logic and conditional flows in 2D on a self-organizing drawing board. The builder can run as a stand-alone CLI tool or be tightly integrated into your project. It works in any modern browser and with mouse, touch or pen. The complete structure of a form is stored in a `JSON` format; the [form definition](guide/builder/#definitions).

[Builder guide](guide/builder/)
{: .start }

### Runner
#### Making your forms fly
You use the supplementary runner library to handily deploy smart forms in websites and applications. It turns a [form definition](guide/builder/#definitions) into an executable program; a finite state machine that handles all the complex logic and response collection during the execution of the form. Apply any UI framework you like. Or pick from the out-of-the-box implementations for [React](https://gitlab.com/{{ site.accounts.gitlab }}/examples/react){:target="_blank"}, [Angular](https://gitlab.com/{{ site.accounts.gitlab }}/examples/angular){:target="_blank"}, [Material-UI](https://gitlab.com/{{ site.accounts.gitlab }}/examples/react-material-ui){:target="_blank"}, [Angular Material](https://gitlab.com/{{ site.accounts.gitlab }}/examples/angular-material){:target="_blank"} and [more](https://gitlab.com/{{ site.accounts.gitlab }}/examples){:target="_blank"}.

[Runner guide](guide/runner/)
{: .start }

### Blocks
#### Taking hardcore charge
You decide which form *building blocks* (e.g. question types) you want to use in the builder and runner. We offer a default set to choose from, but you can also develop your own.

[Blocks guide](guide/blocks/)
{: .start }
