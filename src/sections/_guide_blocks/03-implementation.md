---
source: sections/_guide_blocks/03-implementation.md
bookmark: implementation
name: Implementation
title: Implementing blocks
code: |
  ```json
  {
    "name": "your-block",
    "version": "1.0.0",
    "main": "index.js",
    "scripts": {
      "test": "webpack -d && concurrently -n \"tripetto,webpack\" -c \"blue.bold,green\" -k -s \"first\" \"tripetto ./test/example.json\" \"webpack -d --watch\""
    },
    "devDependencies": {
      "tripetto": "*",
      "concurrently": "*",
      "ts-loader": "*",
      "typescript": "*",
      "webpack": "*",
      "webpack-livereload-plugin": "*"
    },
    "tripetto": {
      "blocks": [
        "."
      ]
    }
  }
  ```
  {: title="package.json" }
  ```javascript
  const webpack = require("webpack");
  const webpackLiveReload = require("webpack-livereload-plugin");

  module.exports = {
    entry: "./index.ts",
    output: {
      filename: "./index.js"
    },
    module: {
      rules: [
        {
          test: /\.ts$/,
          use: "ts-loader"
        }
      ]
    },
    resolve: {
      extensions: [".ts", ".js"]
    },
    externals: {
      "tripetto": "Tripetto"
    },
    plugins: [
      new webpackLiveReload({
        appendScriptTag: true
      })
    ]
  };
  ```
  {: title="webpack.config.js" }
---

Blocks for Tripetto are actually [packages](https://docs.npmjs.com){:target="_blank"}. A package is a directory that is described by a `package.json` and so is a block. The most important part of the package configuration is the entry point of the block. Normally defined by the `main`-field. The entry point is used when the Tripetto builder wants to load your block. If you want you can include multiple blocks in a single package. But we'll assume for now that you create a package for each block.

The minimal package file for a block should look like this:

```json
{
  "name": "your-block",
  "version": "1.0.0",
  "main": "index.js"
}
```

This example assumes the entry point of your block implementation is at `index.js`.

### Publishing and consuming your block
When you have a block with a valid `package.json`, you can publish it to any registry you like. To use your block in the builder, read the instructions on how to use them in the [cli tool](../builder#cli-configuration) or in the [library](../builder#library-blocks).

### Testing during development
You'll want an easy and quick way to test your block while you are building your block. This is achieved by adding a `tripetto`-field with the following content to the `package.json` of your block:

```json
{
  "name": "your-block",
  "version": "1.0.0",
  "main": "index.js",
  "tripetto": {
    "blocks": [
      "."
    ]
  }
}
```

With this the builder will load the block when you start the builder from the block folder. In the boilerplate we combined this feature with the [webpack live reload plugin](https://www.npmjs.com/package/webpack-livereload-plugin){:target="_blank"}. This automatically restarts the builder with each change in the block code.

An example `package.json` with corresponding [webpack](https://webpack.js.org/){:target="_blank"} configuration is displayed to the right (or a bit further down, if you’re reading this on a device with limited screen width) to get you up and running fast.

### Alternative method for the entry point
Instead of supplying the entry point in the `main`-field you may also use the `entry` field of the `tripetto` section to do this. For example:
```json
{
  "name": "your-block",
  "version": "1.0.0",
  "tripetto": {
    "entry": "index.js",
    "blocks": [
      "."
    ]
  }
}
```

This documentation is updated as we continue our work on Tripetto and build a community. Please [let us know](../../support) if you run into issues or need additional information. We’re more than happy to keep you going and also improve the documentation.
{: .warning }
