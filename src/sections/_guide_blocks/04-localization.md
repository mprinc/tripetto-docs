---
source: sections/_guide_blocks/04-localization.md
bookmark: localization
title: Localization
---

We use the [gettext](https://www.gnu.org/software/gettext/gettext.html){:target="_blank"} convention for the localization of Tripetto and its blocks. You should enclose all your labels/texts that need translation using one of the following functions.

##### **_**(*str*, *...arguments*): string
{: .method }
The `_` function is actually an alias for the [`gettext`](https://www.gnu.org/software/gettext/manual/html_node/gettext.html){:target="_blank"} function. We use it because it is shorter and more distinctive. Supply your (to be translated) string as the first argument. Optional arguments can be referenced in your string using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`.

`str`
: Specifies the string that needs to be translated.

`...arguments`
: Optional arguments that can be referenced using `%n` where `n` is the argument number (the first argument is `%1`).

Example:

```typescript
const label = _("Lorem %1", "ipsum");

console.log(label); // Outputs `Lorem ipsum`
```

##### **_n**(*single*, *plural*, *count*, *...arguments*): string
{: .method }
Translates a plural string (short version for [`ngettext`](https://www.gnu.org/software/gettext/manual/html_node/Plural-forms.html){:target="_blank"}).

`single`
: Specifies the string containing the single message that needs to be translated.

`plural`
: Specifies the string containing the plural message that needs to be translated.

`count`
: Specifies the count for the plural.

`...arguments`
: Optional string arguments that can be referenced in the translation string using the percent sign followed by the argument index `%n`. The count value `count` is automatically included as the first argument (`%1`).

Example:

```typescript
const label = _n("1 car", "%1 cars", 2);

console.log(label); // Outputs `2 cars`
```

This documentation is updated as we continue our work on Tripetto and build a community. Please [let us know](../support) if you run into issues or need additional information. We’re more than happy to keep you going and also improve the documentation.
{: .warning }
