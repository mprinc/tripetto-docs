---
source: sections/_guide_blocks/03.07-condition-templates.md
bookmark: condition-templates
name: Condition templates
title: Condition templates
sub: true
---

When you implement a node block, it is possible to supply condition templates to the builder. These templates can be used to allow the creation of actual conditions for your block in the builder with one click. The templates will be shown when the user wants to add a branch to a certain cluster. To make this work you have to add a dedicated method decorated with the `@conditions` decorator to your block class.

```typescript
...
@conditions
defineConditions(): void {
  this.conditions.template({
    condition: ExampleCondition,
    label: "Example condition"
  });
}
...
```

This documentation is updated as we continue our work on Tripetto and build a community. Please [let us know](../support) if you run into issues or need additional information. We’re more than happy to keep you going and also improve the documentation.
{: .warning }
