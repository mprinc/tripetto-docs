---
source: sections/_guide_blocks/03.01-nodes.md
bookmark: nodes
name: Node blocks
title: Implementing a node block
sub: true
code: |
  ```typescript
  import { NodeBlock, Slots, _, builder, slots, tripetto } from "tripetto";
  import * as ICON from "./text.svg";

  @tripetto({
    type: "node",
    identifier: "text",
    icon: ICON,
    label: () => _("Text (single line)")
  })
  export class Text extends NodeBlock {
    public value: Slots.TextSlot;

    @slots
    defineSlot(): void {
      this.value = this.slots.static({
        type: Slots.TextSlot,
        reference: "value",
        label: _("Text value")
     });
    }

    @editor
    defineEditor(): void {
      this.editor.name();
      this.editor.description();
      this.editor.placeholder();
      this.editor.explanation();

      this.editor.groups.options();
      this.editor.required(this.value);
      this.editor.visibility();
      this.editor.alias(this.value);
    }
  }
  ```
---

Node blocks are used to create node building blocks for the builder. The minimal implementation should look like this:

```typescript
import {
  NodeBlock, tripetto
} from "{{ site.npm_packages.builder }}";
import * as ICON from "./icon.svg";

@tripetto({
    type: "node",
    identifier: "example-block",
    icon: ICON,
    label: "Example node block"
  })
export class Example extends NodeBlock {}
```

So, let's walk through the code. We define our node block by implementing the `NodeBlock` abstract class, prefixed with the `@tripetto` decorator to supply (meta) information about the block.

### Next steps
Now that you have a working simple node block, let's add more functionality to it by:

- Implementing [slots](#slots);
- Implementing [feature cards](#feature-cards);
- Implementing [condition templates](#condition-templates).

This documentation is updated as we continue our work on Tripetto and build a community. Please [let us know](../support) if you run into issues or need additional information. We’re more than happy to keep you going and also improve the documentation.
{: .warning }
