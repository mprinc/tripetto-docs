---
source: sections/_guide_runner/03.04-data.md
bookmark: data
name: Data store
title: Using the collected data
sub: true
---

Data is always collected inside an `Instance`. Tripetto works with a slot system where each data component is stored in a separate [slot](../blocks/#slots). The [form definition](../builder/#definitions) contains the <span class='important'>meta information about each slot</span>. More information about slots can be found [here](../blocks/#slots).

The easiest way to retrieve all collected data is through the <span class='definition'>`Export` API</span>. This API contains functions to easily export collected data to handy data formats, like a <span class='important'>fieldset</span> or a <span class='important'>CSV</span> file.

```
import { Export } from "{{ site.npm_packages.runner }}";

// Assuming there is an instance with name `instance`

// Export to CSV
const CSV = Export.CSV(instance);

// Export all exportable fields
const exportableFields = Export.exportables(instance);
```

This documentation is updated as we continue our work on Tripetto and build a community. Please [let us know](../support) if you run into issues or need additional information. We’re more than happy to keep you going and also improve the documentation.
{: .warning }
