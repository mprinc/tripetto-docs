---
source: sections/_guide_runner/02-preparation.md
bookmark: preparation
title: Preparation
---

To use the runner library in your project you should <span class='important'>install</span> the runner package as a dependency using the following command:
```bash
$ npm install {{ site.npm_packages.runner }} --save
```

It contains the <span class='important'>library runtime</span> files as well as the <span class='important'>TypeScript declaration</span> files (typings). When you import a symbol from the library, TypeScript should be able to automatically find the appropriate type definition for you.

#### Setting up your IDE and building your application
We suggest to use [webpack](https://webpack.js.org/){:target="_blank"} for building your website or application. It can bundle the runner runtime with your project. Take a look at one of our [examples](#examples) to see how to configure webpack for this. If you use [webpack](https://webpack.js.org/){:target="_blank"} to bundle your application, you probably want to install the library package as `devDependencies` using `--save-dev` instead of `--save`.

This documentation is updated as we continue our work on Tripetto and build a community. Please [let us know](../../support) if you run into issues or need additional information. We’re more than happy to keep you going and also improve the documentation.
{: .warning }
