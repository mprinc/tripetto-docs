---
source: sections/_guide_runner/05-issues.md
bookmark: issues
title: Issues
---

Run into issues using the runner? Report them [here](https://gitlab.com/{{ site.accounts.gitlab }}/runner/issues){:target="_blank"}.

Or go to the [support page](../../support/) for more support options.
