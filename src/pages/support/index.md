---
base: ../
permalink: /support/
source: pages/support/index.md
title: Support - Tripetto Documentation
heading: Need help?
---

### We keep working on Tripetto
This website contains the information needed for developers to start using Tripetto and hopefully even build on our foundation. We’ll use any constructive feedback to further improve the software.

#### Issues using the Tripetto builder?
Report them [here](https://gitlab.com/{{ site.accounts.gitlab }}/builder/issues){:target="_blank"}.

#### Issues using the Tripetto runner?
Report them [here](https://gitlab.com/{{ site.accounts.gitlab }}/runner-foundation/issues){:target="_blank"}.

#### Chat
Chat with us or the [community](https://spectrum.chat/tripetto){:target="_blank"}.

#### Other questions?
Contact us on [{{ site.email }}](mailto:{{ site.email }}) or use the [form](https://tripetto.com/contact/){:target="_blank"} at [Tripetto.com](https://tripetto.com){:target="_blank"}.
