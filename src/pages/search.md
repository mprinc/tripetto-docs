---
base: ../
permalink: /search/
title: Search results - Tripetto Documentation
custom: true
sitemap: false
---

<section>
  <section>
    <h1>Search results</h1>
    <p><span id="search-process">Loading</span> results <span id="search-query-container" style="display: none;">for "<strong id="search-query"></strong>"</span></p>
    <ul id="search-results"></ul>
  </section>
</section>

<script>
window.data = {
    {% for collection in site.collections %}
        {% for doc in collection.docs %}
            {% if doc.title %}
                {% unless doc.no_index %}
                    {% if added %},{% endif %}
                    {% assign added = false %}
                    "{{ doc.id | slugify }}": {
                        "id": "{{ doc.id | slugify }}",
                        "title": "{{ doc.title | replace: "<punchline />", "Tripetto is a full-fledged form kit" | xml_escape }}",
                        "category": "{{ collection.name | xml_escape }}",
                        "description": "{{ doc.description | xml_escape }}",
                        "type": "{{ doc.type | xml_escape}}",
                        "url": "{{collection.permalink | replace_first: "/", page.base }}#{{ doc.bookmark | default: doc.id | downcase | replace: " / ", "" | replace: ".", "" | xml_escape}}",
                        "content": {{ doc.content | strip_html | replace_regex: "[\s/\n]+"," " | strip | jsonify }}
                    }
                    {% assign added = true %}
                {% endunless %}
            {% endif %}
        {% endfor %}
    {% endfor %}
};
</script>
<script src="{{ page.base }}js/lunr-2.3.1.js"></script>
<script src="{{ page.base }}js/search.js"></script>
